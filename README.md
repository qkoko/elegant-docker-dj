# Elegant Docker Django project

This project follows the medium post from Hendrik Frentrup


[How to Dockerize a Django web app elegantly](https://medium.com/faun/tech-edition-how-to-dockerize-a-django-web-app-elegantly-924c0b83575d "Elegant Docker Django")


## Basic Dockerfile
```Dockerfile
FROM python:3.6.7-alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD ./ /code/
CMD ["python", "mysite/manage.py", "runserver", "0.0.0.0:8001"]
```

## Basic docker-compose.yml
```docker
version: '3'

services:
  web:
    build: .
    command: python mysite/manage.py runserver 0.0.0.0:8001
    ports:
      - "8001:8001"
    volumes:
      - .:/code
```

Run ```python manage.py collectstatic``` to generate static files at _STATIC_ROOT_ to be served by nginx.